﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
	public AudioClip[] clip;
	public int i=0;
	// Use this for initialization
	void Start () {
	if (Application.loadedLevelName == "menu") {
			audio.clip = clip[i];
			i++;
			StartCoroutine("StartAudio");
				}
	}
	
	IEnumerator StartAudio() {
		audio.Play();
		yield return new WaitForSeconds(audio.clip.length);
		audio.clip = clip[i];
		i++;
		audio.Play();
	}

	void Update()
	{
		int i = Application.loadedLevel;
		if (Input.GetKeyDown (KeyCode.L)) {
			Application.LoadLevel(++i);
				}
	}
}
