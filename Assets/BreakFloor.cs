﻿using UnityEngine;
using System.Collections;

public class BreakFloor : MonoBehaviour {


	public Animator animate;
	// Use this for initialization
	void Start () {
		animate.Play("Broken Floor", 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
