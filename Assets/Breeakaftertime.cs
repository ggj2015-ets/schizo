﻿using UnityEngine;
using System.Collections;

public class Breeakaftertime : MonoBehaviour {
	float TimeatStart;
	GameObject TempColObj;
	public AudioSource au;
	public bool floorColapsed = false;

	public GameObject[] explosion;

	// Use this for initialization
	void Start () {
		TimeatStart = Time.time;

	}
	
	// Update is called once per frame
	void Update () {
	if (Time.time - TimeatStart > 1.5) {
			if(!floorColapsed)
			{
				Instantiate(au,gameObject.transform.position,Quaternion.identity);
				floorColapsed = true;
			}				
		}
	if(Time.time - TimeatStart > 3)
		{
	

			foreach(Rigidbody go in gameObject.GetComponentsInChildren<Rigidbody>())
			{

				go.useGravity = true;
				go.AddExplosionForce(-10.0f,go.gameObject.transform.position,1.0f);
			}
			for(int i=0; i<explosion.Length;i++)
			{
				//explosion[i].rigidbody.AddExplosionForce(10000.0f,explosion[i].transform.position,100.0f);
			}

		}
	}
}
