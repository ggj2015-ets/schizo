﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Linq.Expressions;

public class Player: MonoBehaviour {
	//X , Y, Z
	public Vector3 X{
		get{return new Vector3(this.gameObject.transform.position.x,0,0);}
		set{this.gameObject.transform.position = new Vector3(value.x,0,0);}
	}
	public Vector3 Y{
		get{return new Vector3(0,this.gameObject.transform.position.y,0);}
		set{this.gameObject.transform.position = new Vector3(0,value.y,0);}
	}
	public Vector3 Z{
		get{return new Vector3(0,0,this.gameObject.transform.position.z);}
		set{this.gameObject.transform.position = new Vector3(0,0,value.z);}
	}
	
	//Buffs
	public bool IsGravity ;
	public bool IsInvertedLook;
	public bool IsInvertedAxis;
	public bool IsShake;
	public bool isControls;
	bool IsOnGround;
	public float DragonNInput;
	public float DragonInput;
	public float Speed;
	public float JumpStrength;
	public KeyCode Forwards;
	public KeyCode Backwards;
	public KeyCode Left;
	public KeyCode Right;
	public CapsuleCollider PlayerCollider;
	public bool IsTechno;


	// Use this for initialization
	void Start () {
		Screen.lockCursor = true;
	
	} 
	
	// Update is called once per frame
	void Update () {

		RaycastHit hitt;
		IsOnGround =Physics.SphereCast(this.gameObject.transform.position, 1.0f,Vector3.down, out hitt, 5.0f);
		Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f,0.5f,0));
		RaycastHit hit;


		if(isControls)
		{
		if(Input.GetKey(Forwards) && IsOnGround ){

				this.gameObject.rigidbody.AddForce(new Vector3(ray.direction.x,0,ray.direction.z) * Speed , ForceMode.Acceleration);
			this.gameObject.rigidbody.drag = DragonInput;
		}
		else if(Input.GetKey(Backwards)  && IsOnGround ){
			this.gameObject.rigidbody.AddForce(new Vector3(-ray.direction.x,0,-ray.direction.z) * Speed , ForceMode.Acceleration);
			this.gameObject.rigidbody.drag = DragonInput;
		}
		else if(Input.GetKey(Left)  && IsOnGround ){
			this.gameObject.rigidbody.AddRelativeForce(-Vector3.right * Speed );
			this.gameObject.rigidbody.drag = DragonInput /1.2f;
		}
		else if(Input.GetKey(Right)  && IsOnGround){
			this.gameObject.rigidbody.AddRelativeForce(Vector3.right  * Speed  );
			this.gameObject.rigidbody.drag = DragonInput /1.2f;
		}

		else
		{
			this.gameObject.rigidbody.drag = DragonNInput;
		}
		}
 		if(Input.GetKeyDown(KeyCode.Space)  && IsOnGround)
		{
			this.gameObject.rigidbody.AddRelativeForce(Vector3.up * JumpStrength, ForceMode.Force);
		}

		else if(!IsOnGround)
		{
			Physics.gravity =new Vector3(0,-20,0);
		}
		else
		{
			Physics.gravity = new Vector3(0,-9.81f,0);
		}

		//if player movement
		if(this.gameObject.rigidbody.velocity != Vector3.zero)
		{

		}

	}

	void OnCollisionExit (Collision PlayerCollider){

	}

	IEnumerator HaltControls()
	{
		yield return new WaitForSeconds(2f);
		isControls = false;
	   yield return new WaitForSeconds(3f);
		isControls = true;
	
	}
}
