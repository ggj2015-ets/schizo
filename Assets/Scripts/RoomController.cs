using UnityEngine;
using System.Collections;

public class RoomController : MonoBehaviour {

	

	public Player player_state;
	public float ShakeIntensity;
	public Color LightingColor1;
	public Color LightingColor2;
	

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		LightingColor1 = new Color(Random.value,Random.value, Random.value, 1f);
		LightingColor2 = new Color(Random.value,Random.value, Random.value, 1f);

		if (player_state.IsGravity)
		{
			player_state.rigidbody.useGravity = false;
			player_state.rigidbody.AddForce(new Vector3(0,9.8f,0),ForceMode.Acceleration);
	
		}
		else
		{
			player_state.rigidbody.useGravity = true;
		}
		if(player_state.IsInvertedLook && player_state.IsInvertedAxis)
		{
			player_state.Forwards = KeyCode.S;
			player_state.Backwards = KeyCode.W;
			player_state.Left = KeyCode.D;
			player_state.Right = KeyCode.A;
			player_state.gameObject.GetComponent<MouseLook>().sensitivityX = -1;
			GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MouseLook>().sensitivityY = -1;
		}
		else if (player_state.IsInvertedAxis)
		{
			player_state.Forwards = KeyCode.S;
			player_state.Backwards = KeyCode.W;
			player_state.Left = KeyCode.D;
			player_state.Right = KeyCode.A;
		}
		else if(player_state.IsInvertedLook)
		{
			player_state.gameObject.GetComponent<MouseLook>().sensitivityX = -1;
			GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MouseLook>().sensitivityY = -1;
		}
		if(player_state.IsShake)
		{
			GameObject.FindGameObjectWithTag("MainCamera").gameObject.transform.position = (player_state.gameObject.transform.position  + new Vector3(Random.value/ShakeIntensity,Random.value/ShakeIntensity,Random.value/ShakeIntensity));
		}
		if(player_state.IsTechno)
		{
			player_state.gameObject.GetComponentInChildren<Light>().intensity = 2;
			player_state.gameObject.GetComponentInChildren<Light>().color = Color.Lerp(LightingColor1,LightingColor2,1f);
		}else
		{
			player_state.gameObject.GetComponentInChildren<Light>().intensity = 0;
		}

	}



}

