﻿using UnityEngine;
using System.Collections;

public class RaisePillarOnTrigger : MonoBehaviour {
	public GameObject pillar1;
	public GameObject pillar2;
	public GameObject pillar3;

	public Transform pillar1_begin;
	public Transform pillar2_begin;
	public Transform pillar3_begin;
	public Transform pillar1_end;
	public Transform pillar2_end;
	public Transform pillar3_end;

	public bool raise = false;

	public float speed = 1.0F;
	private float startTime;
	private float journeyLength1;
	private float journeyLength2;
	private float journeyLength3;
	public Transform target;
	public float smooth = 5.0F;

	void OnTriggerEnter(Collider other) {
		raise = true;
		startTime = Time.time;
		journeyLength1 = Vector3.Distance(pillar1_begin.position, pillar1_end.position);
		journeyLength2 = Vector3.Distance(pillar2_begin.position, pillar2_end.position);
		journeyLength3 = Vector3.Distance(pillar3_begin.position, pillar3_end.position);
	}
	

	void Update()
	{
		if (raise) {
			float distCovered = (Time.time - startTime) * speed;
			float fracJourney1 = distCovered / journeyLength1;
			float fracJourney2 = distCovered / journeyLength2;
			float fracJourney3 = distCovered / journeyLength3;
			pillar1.transform.position = Vector3.Lerp(pillar1.transform.position, pillar1_end.position, fracJourney1);
			pillar2.transform.position = Vector3.Lerp(pillar2.transform.position, pillar2_end.position, fracJourney2);
			pillar3.transform.position = Vector3.Lerp(pillar3.transform.position, pillar3_end.position, fracJourney3);

				}
	}
	
}
