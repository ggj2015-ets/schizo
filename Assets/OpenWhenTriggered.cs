using UnityEngine;
using System.Collections;

public class OpenWhenTriggered : MonoBehaviour
{ 
		public float speed = 1.0F;
		public bool openDoor = false;
		public int openDoorCount = 0;
		float start = 0.0f;
		public float target = -120.0f;
		float Smooth = 2.0f;


		void OnTriggerEnter (Collider sph)
		{
			if(sph.tag == "Player")
				openDoor = true;
		}

		// Update is called once per frame
		void Update ()
		{		
			if (openDoor) 
			{
		
				Quaternion DoorOpen = Quaternion.Euler (0, 0,target);
				this.gameObject.transform.localRotation = Quaternion.Slerp (transform.localRotation, DoorOpen, Time.deltaTime * Smooth);

			}

		}
}
