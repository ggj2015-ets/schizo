﻿using UnityEngine;
using System.Collections;

public class FadeoutOverTime : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke ("Hide",5.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
