﻿using UnityEngine;
using System.Collections;

public class CompleteOnPlayerCollision : MonoBehaviour {

	public string nextLevel;

	void OnTriggerEnter(Collider other) {
		if(other.tag == "Player" && nextLevel!="")
		{
			Application.LoadLevel(nextLevel);
		}
	}
}
