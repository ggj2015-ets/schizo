﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovewhenApproached : MonoBehaviour {

	public bool isTriggered = false;
	bool isMaxLeft;
	bool isMaxRight;
	public GameObject Runner;
	public GameObject LimitRight;
	public GameObject LimitLeft;

	// Use this for initialization
	void OnTriggerEnter (Collider col)
	{
		isTriggered = true;//-5.6 7.5
		Invoke ("MessageDoor",1.0f);

	}
	void OnTriggerExit (Collider col)
	{
		//isTriggered = false;
	}

	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	
		if(isTriggered && Runner.transform.position.x <=7.4 && !isMaxLeft)
		{
			Runner.transform.position += new Vector3(0.5f,0,0);

		}
		else if(isTriggered && Runner.transform.position.x >=7.4)
		{
			isMaxLeft = true;
		}

		if(isTriggered && isMaxLeft && Runner.transform.position.x >= -5.6)
		{
			Runner.transform.position += new Vector3(-0.5f,0,0);

		}
		else if(isTriggered && Runner.transform.position.x <= -5.6)
		{
			isMaxRight = true;
			isMaxLeft = false;
		}
	}

	public void MessageDoor()
	{
		Runner.GetComponent<ChangeLevelOnCollisionWhenMoving>().moving = true;
	}

}
