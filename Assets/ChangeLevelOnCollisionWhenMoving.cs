﻿using UnityEngine;
using System.Collections;

public class ChangeLevelOnCollisionWhenMoving : MonoBehaviour {
	public bool moving = false;
	// Use this for initialization
	public string nextLevel;
	
	void OnTriggerEnter(Collider other) {
		if(other.tag == "Player" && nextLevel!="" && moving)
		{
			Application.LoadLevel(nextLevel);
		}
	}
}
