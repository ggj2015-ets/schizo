﻿using UnityEngine;
using System.Collections;

public class RaiseStep : MonoBehaviour {

	public GameObject stepParticule;

	public GameObject Target;
	public Collider col;
	bool Triggered = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		if(Target.transform.position.y  <   GameObject.Find(Target.name + " Limit").transform.position.y && Triggered)
		{
			Target.transform.position += new Vector3(0,0.005f,0);
			stepParticule.particleSystem.Play();
		}
		else
			stepParticule.particleSystem.Stop();
	
	}

	void OnTriggerEnter (Collider col)
	{
		Triggered = true;
	}


}
