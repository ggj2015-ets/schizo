﻿using UnityEngine;
using System.Collections;

public class ShowExitMenu : MonoBehaviour {
	public GameObject gameManager;
	void OnTriggerEnter (Collider col)
	{
		//ShowMenu
		Invoke ("TrigMenu", 3.0f);
	}

	public void TrigMenu()
	{
		gameManager.GetComponent<GameManager> ().ShowMenu ();
	}
}
