﻿using UnityEngine;
using System.Collections;

public class SlideWhenTriggered : MonoBehaviour
{
		public Transform to;
		public float speed = 1.0F;
		public bool openDoor = false;
		float start = 0.0f;
		public float target = 120.0f;
	public OpenWhenTriggered first_door;

		void OnTriggerEnter (Collider other)
		{
				openDoor = true;
		}
		

		// Update is called once per frame
		void Update ()
		{		
				if (openDoor && first_door.openDoor) {

			
			transform.position = Vector3.Lerp (transform.position, to.position, Time.deltaTime);

			if (Mathf.Abs((transform.position-to.position).magnitude) < 0.1f) {
								openDoor = false;
						}
				}
		}
}
