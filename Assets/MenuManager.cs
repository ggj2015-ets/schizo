﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {
	public GameObject menuAnimator;
	public void ClickPlay()
	{
		menuAnimator.GetComponent<Animator> ().SetTrigger ("StartAnim");
		Invoke ("StartGame", 5.0f);


	}

	public void ClickQuit()
	{
		Application.Quit ();
	}

	public void StartGame()
	{
		Application.LoadLevel ("TheBedroom1");
	}
}
