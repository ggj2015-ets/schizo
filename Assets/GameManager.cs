﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
		public GameObject menu;
		public bool isInMenu = false;
		public float initialTime;
		// Use this for initialization
		void Start ()
		{
				foreach (Transform tr in GetComponentInChildren<Transform> ()) {
						if (tr.name == "Canvas") {
								menu = tr.gameObject;
						}

				}

		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Escape)) {
						ShowMenu ();
				}

				if (isInMenu) {
						if (Input.GetKeyDown (KeyCode.Return)) {
								menu.SetActive (false);
								Time.timeScale = initialTime;
								isInMenu = false;
						}
						if (Input.GetKeyDown (KeyCode.Q)) {
								Application.Quit ();
								menu.SetActive (false);
								Time.timeScale = initialTime;
								isInMenu = false;
						}
				}
		}

		public void ShowMenu ()
		{
				menu.SetActive (true);
				initialTime = Time.timeScale;
				Time.timeScale = 0.0f;
				isInMenu = true;
		}
}
