﻿using UnityEngine;
using System.Collections;

public class WakeOnLoad : MonoBehaviour
{
		public Transform from;
		public Transform to;
		public float speed = 1.0F;
		public bool wake = false;
		public MouseLook mouseLook;
		bool firstUpdate = true;
		Player player;

		// Use this for initialization
		void Start ()
		{
				mouseLook = GetComponent<MouseLook> ();
				mouseLook.enabled = false;

				foreach (MouseLook ml in GetComponentsInChildren<MouseLook> ()) {
						ml.enabled = false;
				}

		player = GetComponent<Player> ();

		}
	
		// Update is called once per frame
		void Update ()
		{
				if (firstUpdate) {
						wake = true;
						firstUpdate = false;
				}

				if (wake) {
						player.isControls = false;
						transform.rotation = Quaternion.Lerp (transform.rotation, to.rotation, Time.deltaTime * speed);

						if (Quaternion.Angle (transform.rotation, to.rotation) < 1) {
								wake = false;
								player.isControls = true;
								mouseLook.enabled = true;

								foreach (MouseLook ml in GetComponentsInChildren<MouseLook> ()) {
										ml.enabled = true;
								}
						}
				}
		}
}
