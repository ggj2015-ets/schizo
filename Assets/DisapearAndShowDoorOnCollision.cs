﻿using UnityEngine;
using System.Collections;

public class DisapearAndShowDoorOnCollision : MonoBehaviour
{
		public GameObject correctDoor;
	bool collided = false;

		// Use this for initialization
		void Start ()
		{
				correctDoor.SetActive (false);
		}
	
		void OnTriggerEnter (Collider other)
		{
				if (other.tag == "Player") {
						correctDoor.SetActive (true);
			collided = true;
				}
		}

		void Update ()
		{
		if (collided) {
			renderer.material.color = new Color (renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, Mathf.Lerp (0.0f, 1.0f, Time.time));

						if (renderer.material.color.a <= 0.1f)
								gameObject.SetActive (false);
				}
		}
}
