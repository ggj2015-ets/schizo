﻿using UnityEngine;
using System.Collections;

public class PlayAudioOnTrig : MonoBehaviour
{
		public AudioSource au;
		public bool isDone = false;

		void OnTriggerEnter (Collider col)
		{
				if (!isDone) {
						Instantiate (au, gameObject.transform.position, Quaternion.identity);
						isDone = true;
				}
		
		}
}
